'use strict';
const bcrypt = require('bcrypt')
const encrypt = (password) => bcrypt.hashSync(password, 10);

module.exports = {
  up: async (queryInterface, Sequelize) => {
     await queryInterface.bulkInsert('Admins', [{
     username: 'Admin',
     password: encrypt('1234'),
     createdAt: new Date,
     updatedAt: new Date
     }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Admins', null, {});
  }
};
